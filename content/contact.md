---
title: Contact
---
Please {{< fa envelope >}} [Email](alex.wermer-colan@temple.edu) me for consultations or other requests.

See my work on {{< fa fab github >}} [Github](https://github.com/hawc2)  

Check my {{< fa fab twitter >}} [Twitter](https://twitter.com/AlexWermerColan) for new stuff
