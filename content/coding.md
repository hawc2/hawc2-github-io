---
title: Coding
---

Various scripts, computing projects, Github resources, etc.

# Collaborative Projects

Editor, Programming Historian in English

Contributor, Philly Community Wireless website and Docs

Digital Scholarship Coordinator, Scholars Studio

# Jupyter Notebooks

Text Analysis With Python Jupyter Notebooks

ContentDM IIIF Collections as Data Jupyter Notebooks
    [Collections as Data](https://github.com/hawc2/collections-as-data-notebooks)
    [ContentDM IIIF](https://github.com/hawc2/contentdm-iiif-api)
    
Querying and Visualizing Wikidata

# DevOps

Data Capsule
