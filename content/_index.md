---
title: Home
---

> Let a little less last - Gertrude Stein

## Introductions

{{< figure class="avatar" src="/images/headshot.jpg" alt="headshot">}}

Hi, I'm testing out building a professional website from scratch. This is a work in progress. Visit www.alexwermercolan.com for my primary website.

## About Me

I am a writer, editor, and translator. I also work as a volunteer as the Administrative Lead of the Philly Community Wireless project.

For my day job, I work as a Digital Scholarship Coordinator in Temple University Libraries' Loretta C. Duckworth Scholars Studio.

## Professional Background

My scholarly and creative interests always circle around the intersection of politics, aesthetics, and technology.

I think of my work in terms of translation, no matter if I am editing books of scholarly criticism and literary history, curating archival materials with emerging technologies for web and immersive visualization, working as a dramaturg to adapt fiction to new media theatrical performances, or translating French poetry into English. I am always concerned with how issues of power and social justice impede access to information and culture, and how these failures and limitations to communication is instrumental to the decline in social infrastructure and the global environment today.

I work on projects that involve artistic interventions into political issues, as well as critical essays on ideological stalemates in historical periods. I am also working on reparative projects, remediating marginalized archival materials, documenting neglected works of art like black art in Philly, or contributing to the growth of a community-owned wireless internet network.

## Portfolio


My creative work represented here ranges from translation of literary works to their adaptation for the theater.

{{< figure class="avatar" src="/avatar.png" alt="avatar">}}

For more info about my academic scholarship and related professional experience, please visit my [CV](/cv).

For my creative work in translation, dramaturgy, and digital new media arts, see my [Portfolio](/portfolio).
